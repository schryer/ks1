from collections import namedtuple

ListStats = namedtuple('ListStats', ['mean', 'high_value', 'low_value', 'range',
                                     'range_over_mean', 'standard_deviation', 'median', 'N', 'sum', 'tag'])


def list_stats(number_iterable, tag=None):
    '''
    Evaluates a number of statistics for a list of numerical values.

    Parameters
    ==========

    number_iterable : any object that can be turned into a numpy.array
                      numpy.array(number_iterable)
    tag: str
         Optional information to include in the return object.

    Returns
    =======

    tuple_of_stats : ListStats namedtuple
    '''

    values = numpy.array(number_iterable)

    try:
        mean = values.mean()
    except Exception as e:
        print('Error evaluating mean for: {}'.format(number_iterable))
        raise e

    try:
        hv = values.max()
    except ValueError as e:
        print('Error evaluating max for: {}'.format(number_iterable))
        raise e
    except Exception as e:
        raise e

    lv = values.min()
    drange = hv - lv

    return ListStats(mean, hv, lv, drange, drange / float(mean), numpy.std(values),
                     numpy.median(values), values.size, values.sum(), tag)


def in_ipynb():
    try:
        cfg = get_ipython().config 
        if cfg['IPKernelApp'].get('connection_file'):
            return True
        else:
            return False
    except NameError:
        return False

